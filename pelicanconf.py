import collections
from collections.abc import Iterable
import six

AUTHOR = '9ekaitz'
SITENAME = '9ekaitz'
SITEURL = 'http://127.0.0.1:8000'

THEME = 'theme/custom'

PATH = 'content'
DIRECT_TEMPLATES = ['index', 'authors', 'tags', 'parking', 'about']
STATIC_EXCLUDES = ['theme/sass']

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = 'en'
LOCALE = 'en_US.utf8'

PLUGINS = ['metadataparsing','i18n_subsites']

JINJA_ENVIRONMENT = {
  'extensions': ['jinja2.ext.i18n']
}

I18N_SUBSITES = {
    'eu': {
        'SITEURL': SITEURL+'/eu',
        'LOCALE': 'eu_ES.utf8'
        }
    }



# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

SourceCodeInfo = []
Repo = collections.namedtuple("Repo", ["url", "remote", "text"])
def parse_source(string):
  if string is None or not isinstance(string, Iterable):
    return None
  if not isinstance(string, six.string_types):
    return None
  
  data = string.split(",")

  max = len(data)
  if max % 3 != 0:
    max -= max % 3

  if max < 3:
    return None
  
  SourceCodeInfo = [Repo(data[i],data[i+1], data[i+2]) for i in range(0,max-2,3)]
  return SourceCodeInfo

def parse_technologies(string):
  if string is None or not isinstance(string, Iterable):
    return None
  
  return string.split(",")

METADATA_PARSERS = {
    "Sources": parse_source,
    "Tech": parse_technologies
}