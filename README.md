# 9ekaitz.eus

My website, a simple web space to display projects and articles without using javascript. Available at [9ekaitz.eus](https://9ekaitz.eus)

## Description

The site uses [Pelican](https://blog.getpelican.com/) to statically generate the content based on a template. The template is custom made following the material design guidelines. The website is fully responsive and dark-themed.

I may add a light theme with a toggle in the future, but for the moment it's all just a beautifull dark theme 😃.

The `pelican-metadataparsing' and 'i18n_subsites' plugins are used to customize the info in the articles and to generate the site in two languages (eu and en).

## Pelican theme

The theme is in the folder `theme/custom`. It currently has some hardcoded info like the description and the nav bar. I'll try to extract the hardcoded bits and allow customization using the file `pelicanconf.py`, but for the moment it can be reused by removing the description segment and replacing the nav logo and entries with the ones needed. This needs to be done for the files 'base.html' and 'index.html' inside the theme folder.