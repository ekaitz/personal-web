# Translations template for PROJECT.
# Copyright (C) 2022 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2022.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2022-06-27 20:45+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.9.1\n"

#: templates/article.html:16
msgid "Home"
msgstr ""

#: templates/article.html:16
msgid "Projects"
msgstr ""

#: templates/base.html:96
msgid " PROJECTS "
msgstr ""

#: templates/base.html:101
msgid "ARTICLES"
msgstr ""

#: templates/base.html:106
msgid "ABOUT"
msgstr ""

#: templates/base.html:129
msgid ""
"Proudly powered by <a class=\"pelican-link\" "
"href=\"https://getpelican.com\">Pelican</a>"
msgstr ""

#: templates/index.html:8
msgid "Hi, I'm Ekaitz"
msgstr ""

#: templates/index.html:11
msgid ""
"I'm currently finishing my<br />\n"
"      Bachelor's Degree in computer engineering<br />\n"
"      I'm a FOSS and cybersecurity enthusiast"
msgstr ""

#: templates/index.html:26
msgid "Other projects"
msgstr ""

#: templates/index.html:48 templates/project.html:16
msgid "Published:"
msgstr ""

#: templates/index.html:51
msgid "read more"
msgstr ""

#: templates/project.html:6
msgid "Technologies:"
msgstr ""

#: templates/project_info.html:14
msgid "Category:"
msgstr ""

#: templates/project_info.html:21
msgid "Tags:"
msgstr ""

#: templates/project_info.html:29
msgid "Source code"
msgstr ""

#: templates/project_info.html:29
msgid "and binaries"
msgstr ""

#: templates/project_info.html:30
msgid "available at:"
msgstr ""

